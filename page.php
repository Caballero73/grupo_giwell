<!-- Archivo de cabecera gobal de Wordpress -->
<?php get_header(); ?>
<!-- Contenido de las páginas que se agregan al sitio -->
<div class="container">
  	<div class="row">
	    <div class="col-sm-9 col-xs-12">
			<?php if ( have_posts() ) : the_post(); ?>
				<section>
				  <h1><?php the_title(); ?></h1>
				  <?php the_content(); ?>
				</section>
			<?php endif; ?>
		</div><!-- /.col-sm-9 col-xs-12 -->

		<div class="col-sm-3 col-xs-12 fondo">
			<!-- Archivo de barra lateral por defecto -->
			<?php get_sidebar(); ?>
		</div><!-- /.col-sm-3 col-xs-12 -->
	</div><!-- /.row -->
</div><!-- /.container -->
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>