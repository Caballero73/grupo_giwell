    </div><!-- /. Fin Container: Contenido del sitio -->

    <center>    	    
	    <footer id="pie">
		<!--<nav>
	        <ul class="legal-nav">
	          <?php wp_nav_menu( array( 'theme_location' => 'legal' ) ); ?>
	        </ul>
	      </nav>-->
	      <small>Enrique Valerio &copy; <?php echo date("Y") ?></small>
	    </footer>
	    <?php wp_footer(); ?>
	</center>

    <!-- JAVASCRIP CORE -->
    <script src="<?php bloginfo('template_url') ?>/js/jquery-2.1.1.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/js/npm.js"></script>
  </body>
</html>