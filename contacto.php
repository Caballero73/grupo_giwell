<?php
/**
 * Template Name: Plantilla de Contacto
 */ 
?>

<!-- Archivo de cabecera gobal de Wordpress -->
<?php get_header(); ?>
<div class="container">
  <div class="row">
    <div class="col-sm-8 col-xs-12">
    <!-- Contenido de página de inicio -->
    <?php if ( have_posts() ) : the_post(); ?>
    <section>
      <h1><?php the_title(); ?></h1>
      <?php the_content(); ?>
      <form method="post" action="">
          <label for="name">Nombre</label><br>
          <input type="text" id="name" placeholder="Escribe aquí tu nombre y apellidos"><br>
          <label for="subject">Asunto</label><br>
          <input type="text" id="subject" placeholder="Motivo de tu consulta"><br>     
          <label for="message">Mensaje</label><br>
          <textarea id="message"></textarea><br>
          <button type="submit">Enviar datos</button><br>
      </form>
    </section>
    <?php endif; ?>
    </div><!-- /. Columna -->

    <div class="col-sm-4 col-xs-12 fondo">    
      <!-- Archivo de barra lateral por defecto -->
      <?php get_sidebar(); ?>
    </div><!-- /. Columna -->

  </div><!-- /. Row -->

<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>