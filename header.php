<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Sitio Oficial del Grupo Gilwell del Distrito Scout Paraguaná" />
	<meta name="keywords" content="grupo gilwell, insignia de madera, scout, scouts, scouters" />
	<meta name="author" content="Scouter Enrique" />
	<link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/favicon.ico">
	<!-- CSS Core -->
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/font-awesome.min.css">
	<!-- Tema de Boostrap -->
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/bootstrap-theme.min.css">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Palanquin+Dark" rel="stylesheet">

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
    <?php wp_head(); ?>
  </head>
  
  <!-- Se sustituyó la etiqueta <body> para mejorar la integración con WP	 -->
  <?php echo '<body class="'.join(' ', get_body_class()).'">'.PHP_EOL; ?>
  
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    	<div class="container">    		
    		
	    	<!-- El logotipo y el icono que despliega el menú se agrupan
	    	para mostrarlos mejor en los dispositivos móviles -->
			<div class="navbar-header">
			
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				  <span class="sr-only">Móvil</span>
				  <i class="fa fa-bars"></i>
				</button>
						
			</div>

			<!-- Agrupar los enlaces de navegación, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="main-nav">
				  <?php wp_nav_menu( array( 'theme_location' => 'navegation' ) ); ?>
				</ul>
				<hr class="visible-xs">
				<hr class="visible-xs">
				<!-- Buscador -->
				<div class="buscador pull-right">
				    <?php get_search_form();?>
				</div>
			</div>
		</div>
    </nav>

	<!-- Contenido del sitio -->
