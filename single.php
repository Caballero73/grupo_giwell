<!-- Página que muestra un Post determinado -->
<!-- Archivo de cabecera gobal de Wordpress -->
<?php get_header(); ?>
<div class="container">
<div class="row">
    <div class="col-sm-9 col-xs-12">
      <!-- Contenido del post -->
      <?php if ( have_posts() ) : the_post(); ?>
        <section>
            <header>
              <h1><?php the_title(); ?></h1>
              <ul class="nav nav-pills fa">
                <li><time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j F, Y'); ?></time></li>
                <li><?php the_category (); ?></li>
              </ul>
            </header>
            <?php the_content(); ?>
            <div class="row">
              <address class="col-xs-12 col-sm-4">
                <i class="fa fa-user-circle" aria-hidden="true"></i> <?php the_author_posts_link() ?>
              </address>
              <div class="col-xs-12 col-sm-8 etiqueta fa">
                <?php the_tags('<ul><li>','</li><li>','</li></ul>'); ?>
              </div>
              <div class="clearfix"></div>
              <hr>
              <!-- Comentarios -->
              <div class="alert alert-warning">
                <?php comments_template(); ?>
              </div>
        </section>
      <?php else : ?>
        <p><?php _e('Ups!, esta entrada no existe.'); ?></p>
      <?php endif; ?>
    </div><!-- /. Columna Comentarios -->

    <div class="col-sm-3 col-xs-12 fondo">
      <!-- Archivo de barra lateral por defecto -->
      <?php get_sidebar(); ?>
    </div><!-- /. Columna SideBar -->
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>