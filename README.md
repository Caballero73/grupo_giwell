# GRUPO GIWELL #

Este es un tema de Word Press para el Grupo Giwell de los Scouts en Falcón.

### ¿Para qué es este repositorio? ###

* Compartir este tema con cualquier Scout u otra organización a fin que lo quiera.
* Versión 1.0
* Sitio Scoutsfalcon http://www.scoutsfalcon.org

### ¿Cómo me instalo? ###

* Es un tema de Word Press, y se instala como tal

### Lineamientos de contribución ###

* Pruebas de escritura
* Revisión de código
* Otras pautas

### ¿Con quién hablo? ###

* Enrique Valerio scouter.enrique@gmail.com