<!-- Listado de posts según su Etiqueta-->
<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<div class="container">
  <div class="row">
    <div class="col-sm-9 col-xs-12">
        <!-- Título de etiqueta -->
        <p class="lead text-warning"><b>Etiqueta <?php single_cat_title(); ?></b></p>
        <!-- Listado de posts -->
        <?php if ( have_posts() ) : ?>
        <section>
          <?php while ( have_posts() ) : the_post(); ?>
            <article>
              <header>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j F, Y'); ?></time>
                <div class="btn-group fa"><?php the_category (); ?></div>
              </header>
              <?php the_excerpt(); ?>
              <footer id="footer_categorias">
                <div class="row">
                  <address class="col-xs-12 col-sm-4">
                    <i class="fa fa-user-circle" aria-hidden="true"></i> <?php the_author_posts_link() ?>
                  </address>
                  <div class="col-xs-12 col-sm-8 etiqueta fa">
                    <?php the_tags('<ul><li>','</li><li>','</li></ul>'); ?>
                  </div>
                </div>
              </footer>
            </article>
          <?php endwhile; ?>
          <div class="pagination">
            <span class="in-left"><?php next_posts_link('« Entradas antiguas'); ?></span>
            <span class="in-right"><?php previous_posts_link('Entradas más recientes »'); ?></span>
          </div>
        </section>
        <?php else : ?>
          <p><?php _e('Ups!, no hay entradas.'); ?></p>
        <?php endif; ?>
    </div><!-- /.col-sm-9 col-xs-12 -->
    <div class="col-sm-3 col-xs-12 fondo">
      <!-- Archivo de barra lateral por defecto -->
      <?php get_sidebar(); ?>
    </div><!-- /.col-sm-3 col-xs-12 -->
  </div><!-- /.row -->
</div><!-- /.container -->
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>