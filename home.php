<!-- Sección del Blog -->
<!-- Archivo de cabecera global de Wordpress -->
<?php get_header(); ?>
<div class="container">
  <div class="row">
    <div class="col-sm-9 col-xs-12">
      <!-- Listado de posts -->
      <?php if ( have_posts() ) : ?>
        <section>
          <?php while ( have_posts() ) : the_post(); ?>
            <article>
              <header>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <time datatime="<?php the_time('Y-m-j'); ?>"><?php the_time('j F, Y'); ?></time>
                | <i class="fa fa-user-circle" aria-hidden="true"></i> <?php the_author_posts_link() ?>
                <div class="btn-group fa"> <?php the_category (); ?></div>
              </header>
              <br/>
              <?php the_excerpt(); ?>
              <footer class="fa">
                  <?php the_tags('<ul><li>','</li><li>','</li></ul>'); ?>
              </footer>
            </article>
          <?php endwhile; ?>
          <div class="pagination" style="width: 100%">
            <span class="alignleft"><?php next_posts_link('« Entradas antiguas'); ?></span>
            <span class="pull-right"><?php previous_posts_link('Entradas más recientes »'); ?></span>
          </div>
        </section>
      <?php else : ?>
        <p><?php _e('Ups!, no hay entradas.'); ?></p>
      <?php endif; ?>
    </div><!-- /. Columna -->

    <div class="col-sm-3 col-xs-12 fondo">
      <!-- Archivo de barra lateral por defecto -->
      <?php get_sidebar(); ?>
    </div><!-- /. Columna -->
  </div>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>