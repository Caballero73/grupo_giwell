<!-- //// Este es el archivo que da origen a la página estática de inicio cuando no se usa el Blog como tal //// -->

<!-- Archivo de cabecera gobal de Wordpress -->
<?php get_header(); ?>
<!-- Contenido de página de inicio -->
<header id="cabecera">
	<div class="container">
    	<!-- Título manejado con WP -->
    	<h1 id="titulo1"><?php bloginfo('name'); ?></h1>
    </div>
</header>
<div class="container">

<div class="row">
	<div class="col-sm-8 col-xs-12"> 
		<?php if ( have_posts() ) : the_post(); ?>
		<section style="padding-top: 50px">
			<p class="lead">
				Los integrantes del <b>Grupo Gilwell</b> o Grupo N° 1 de Gilwell no son miembros de una casta superior, sino que son un conjunto de dirigentes que han iniciado su adiestramiento y que deben tener como lema la frase "Saber para servir", con la conciencia de que su permanencia en el Movimiento tiene el único objeto de ayudar a la formación del carácter de los muchachos, para que se conviertan en buenos ciudadanos, evitando creerse merecedores de homenajes y servidumbres.</p>
				<p>No es, como a veces se dice, una especie de clase superior de dirigentes scouts, sino un lazo que une a aquellos que participan de una experiencia común: la Insignia de Madera. Todos aquellos por tanto, que la consiguen, sea en Gilwell o en cualquier otro campo Escuela y que asuman la responsabilidad de dar ejemplo como buenos dirigentes scouts, son miembros del Grupo Nº1 de Gilwell o simplemente <b>Grupo Gilwell</b>.</p>
				<h3 class="text-center">LA ÚNICA RAZÓN DE SER DEL ADULTO EN EL MOVIMIENTO SCOUT SON LOS MUCHACHOS</h3>
		  <!-- Título de la pagina manejado con WP 
		  <h1><?php the_title(); ?></h1>-->
		  <!-- Contenido de la pagina manejado con WP
		  <?php the_content(); ?>-->
		</section>
		<?php endif; ?>
	</div>
	<div class="col-sm-4 col-xs-12" style="background:#e2d2d0;">
		<!-- Archivo de barra lateral por defecto -->
		<?php get_sidebar(); ?>
	</div>
</div>
<!-- Archivo de pié global de Wordpress -->
<?php get_footer(); ?>